#pragma once
class CViewAngles
{
public:
	float y;
	float x;
	void SetAngles(float _x, float _y)
	{
		SetX(_x);
		SetY(_y);
	}
	void SetX(float _x)
	{
		if (_x > 80)
			_x = 80;
		else if (_x < -80)
			_x = -80;

		x = _x / 57.29577951308f;
	}
	void SetY(float _y)
	{
		while (_y > 360)
			_y -= 360;
		while (_y < -360)
			_y += 360;

		y = _y / 57.29577951308f;
	}
};