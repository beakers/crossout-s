#pragma once
class CPlayerList
{
public:
	int32_t GetPlayerNum(int32_t playerIdx)
	{
		return *reinterpret_cast<int32_t*>(reinterpret_cast<uintptr_t>(this) + ((188 * playerIdx) * 4));
	}
	int32_t GetPlayerIndexByNum(int32_t num)
	{
		for (int i = 0; i < 32; ++i)
		{
			auto c_num = *reinterpret_cast<int32_t*>(reinterpret_cast<uintptr_t>(this) + ((188 * i) * 4));
			if (c_num == num)
				return i;
		}
		return -1;
	}
};