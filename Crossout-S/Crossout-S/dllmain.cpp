#include "CommonImports.h"
#include "Utils.h"
#include "CCarList.h"
#include "CPlayerInfoList.h"
#include "CEntityList.h"
#include "CPlayerList.h"
#include "CViewAngles.h"
HMODULE g_hModule;

CViewAngles* viewangles;
CCarList* car_list;
CEntityList* entity_list;
CPlayerInfoList* player_list_info;
CPlayerList* player_list;

void __cdecl FrameUpdate()
{

}

uintptr_t frame_update_escape;

__declspec(naked) void hkFrameUpdate()
{
	DWORD pThis;
	__asm {
		mov pThis, ecx
		pushad
	};

	FrameUpdate();

	__asm {
		popad
		push    ebp
		mov     ebp, esp
		and     esp, 0xFFFFFFF0
		jmp frame_update_escape
	}
}

void MainThread()
{
	Utils::LogSystem_Initialize();

	car_list = *reinterpret_cast<CCarList**>(Utils::FindSignature("Crossout-D3D9.exe", "B9 ? ? ? ? E8 ? ? ? ? 8B 0E") + 1);

	player_list_info = *reinterpret_cast<CPlayerInfoList**>(Utils::FindSignature("Crossout-D3D9.exe", "81 C7 ? ? ? ? E8 ? ? ? ? C7 44 24") + 2);

	entity_list = **reinterpret_cast<CEntityList***>(Utils::FindSignature("Crossout-D3D9.exe", "8B 0D ? ? ? ? 33 F6 8B 81 ? ? ? ? 89 44 24 10") + 2);

	player_list = *reinterpret_cast<CPlayerList**>(Utils::FindSignature("Crossout-D3D9.exe", "05 ? ? ? ? 8B 10 83 FA FF 74 40") + 1);

	viewangles = *reinterpret_cast<CViewAngles**>(Utils::FindSignature("Crossout-D3D9.exe", "F3 0F 10 15 ? ? ? ? 81 EC ? ? ? ? 56") + 4);

	/*Hook frame update*/
	char buffer[6];

	auto frame_update_fn = Utils::FindSignature("Crossout-D3D9.exe", "55 8B EC 83 E4 F0 83 EC 48 56 57 8B F9 89 7C 24 18 68 ? ? ? ? E8 ? ? ? ? 85 C0 74 F2 50 E8 ? ? ? ? 83 3D");
	DWORD dwProtectionBackup;
	VirtualProtect((LPVOID)frame_update_fn, 6, PAGE_EXECUTE_READWRITE, &dwProtectionBackup);
	
	memcpy(buffer, (LPVOID)frame_update_fn, 6);
	
	*(BYTE*)(frame_update_fn) = 0xE9;
	*(DWORD*)(frame_update_fn + 1) = (DWORD)hkFrameUpdate - frame_update_fn - 5;
	VirtualProtect((LPVOID)frame_update_fn, 6, dwProtectionBackup, &dwProtectionBackup);
	frame_update_escape = frame_update_fn + 6;

	Utils::Log("%s loaded!", "Crossout");
	Utils::Log("Press F8 to unload...");

	while (true)
	{
		if (GetAsyncKeyState(VK_F8))
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(500));

			VirtualProtect((LPVOID)frame_update_fn, 6, PAGE_EXECUTE_READWRITE, &dwProtectionBackup);

			memcpy((LPVOID)frame_update_fn, buffer, 6);

			VirtualProtect((LPVOID)frame_update_fn, 6, dwProtectionBackup, &dwProtectionBackup);

			Utils::Log("You can close this window now!");
			Utils::LogSystem_Cleanup();
			FreeLibraryAndExitThread(g_hModule, 0);
		}
		Utils::Sleep(250);
	}
}

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		g_hModule = hModule;
		DisableThreadLibraryCalls(hModule);
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)MainThread, NULL, 0, NULL);
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}