#pragma once
#include "Utils.h"

enum KeyState
{
	None = 0,
	LPM = 2,
	RPM = 4,
	All = 6
};

enum ClassID
{
	AmbientSound = 848,
	AnimatedEntity = 1072,
	AutoGun = 5872,
	Car = 49504,
	CarPart = 2288,
	EffectEntity = 992
};

class CClientClass
{
public:
	const char* szClassName;
	ClassID iClassID;
};

class CVector
{
public:
	float x;
	float y;
	float z;
	CVector(float _x, float _y, float _z)
	{
		x = _x;
		y = _y;
		z = _z;
	}
};

class CVectorInfo
{
public:
	char pad_0x0000[0x30];
	CVector m_vecPosition;
};

class CEntity
{
public:
	char pad_0x0000[0x4];
	CClientClass* pClientClass; 
	char pad_0x00022[0x30]; 
	int player_num;
	char pad_0x0008[0x18];
	unsigned char m_MoventFlag; 
	char pad_0x0055[0xB]; 
	char szPlayerName[16];
	char pad_0x0070[0x48]; 
	float flHealth; 
	float flMaxHealth; 
	char pad_0x00C1[0x28];
	int m_Team;
	char pad_0x00C2[0x44]; 
	CVectorInfo* pVectorInfo;
	char pad_0x00C0[0x6D8];
	KeyState m_KeyState;  
	char pad_0x080D[0x3]; 
	bool m_HandBrake;
}; 

