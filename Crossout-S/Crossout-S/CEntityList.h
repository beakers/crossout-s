#pragma once
#include "Utils.h"
class CEntityList
{
public:
	int32_t GetEntitiesCount()
	{
		static auto entites_offset = *reinterpret_cast<uintptr_t*>(Utils::FindSignature("Crossout-D3D9.exe", "8B 81 ? ? ? ? 89 44 24 10 85 C0") + 2);

		return *reinterpret_cast<int32_t*>(reinterpret_cast<uintptr_t>(this) + entites_offset);
	}

	CEntity* GetPlayerCarById(int32_t playerId)
	{
		static auto car_offset = *reinterpret_cast<uintptr_t*>(Utils::FindSignature("Crossout-D3D9.exe", "05 ? ? ? ? 8D 0C 40 A1 ? ? ? ? 8D 34 88") + 1);
		return *reinterpret_cast<CEntity**> (reinterpret_cast<uintptr_t>(this) + 12 * ((playerId & 0xFFF) + car_offset));
	}

	CEntity* GetEntityByIndex(int32_t idx)
	{
		static auto entites_offset = *reinterpret_cast<uintptr_t*>(Utils::FindSignature("Crossout-D3D9.exe", "8B 81 ? ? ? ? 89 44 24 10 85 C0") + 2);

		return *reinterpret_cast<CEntity**>(reinterpret_cast<uintptr_t>(this) + entites_offset + 0x8 + (idx * 0xC));
	}
};