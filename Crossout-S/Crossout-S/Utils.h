#pragma once
#include "CommonImports.h"
#include <Psapi.h>
#include <time.h>
class Utils
{
public:

	static void LogSystem_Initialize()
	{
		AllocConsole();

		freopen("CONIN$", "r", stdin);
		freopen("CONOUT$", "w", stdout);
		freopen("CONOUT$", "w", stderr);

		char nBuffer[128];
		sprintf(nBuffer, "%s - Build from %s @ %s", "Crossout", __DATE__, __TIME__);
		SetConsoleTitleA(nBuffer);
	}

	static void LogSystem_Cleanup()
	{
		FreeConsole();
	}

	static void Log(char* fmt, ...)
	{
		va_list va_alist;
		char logBuf[256] = { 0 };

		va_start(va_alist, fmt);
		_vsnprintf(logBuf + strlen(logBuf), sizeof(logBuf) - strlen(logBuf), fmt, va_alist);
		va_end(va_alist);

		if (logBuf[0] != '\0')
		{
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), (FOREGROUND_GREEN | FOREGROUND_INTENSITY));
			printf("[%s]", GetTimeString().c_str());
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), (FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN));
			printf(": %s\n", logBuf);
		}
	}

	static void Sleep(int Ms)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(Ms));
	}

	static std::string GetTimeString()
	{
		time_t current_time;
		struct tm *time_info;
		static char timeString[10];

		time(&current_time);
		time_info = localtime(&current_time);

		strftime(timeString, sizeof(timeString), "%I:%M %p", time_info);
		return timeString;
	}

	static uint64_t FindSignature(const char* szModule, const char* szSignature)
	{
#define INRANGE(x,a,b)  (x >= a && x <= b) 
#define getBits( x )    (INRANGE((x&(~0x20)),'A','F') ? ((x&(~0x20)) - 'A' + 0xa) : (INRANGE(x,'0','9') ? x - '0' : 0))
#define getByte( x )    (getBits(x[0]) << 4 | getBits(x[1]))

		MODULEINFO modInfo;
		GetModuleInformation(GetCurrentProcess(), GetModuleHandleA(szModule), &modInfo, sizeof(MODULEINFO));
		DWORD startAddress = (DWORD)modInfo.lpBaseOfDll;
		DWORD endAddress = startAddress + modInfo.SizeOfImage;
		const char* pat = szSignature;
		DWORD firstMatch = 0;
		for (DWORD pCur = startAddress; pCur < endAddress; pCur++) {
			if (!*pat) return firstMatch;
			if (*(PBYTE)pat == '\?' || *(BYTE*)pCur == getByte(pat)) {
				if (!firstMatch) firstMatch = pCur;
				if (!pat[2]) return firstMatch;
				if (*(PWORD)pat == '\?\?' || *(PBYTE)pat != '\?') pat += 3;
				else pat += 2;
			}
			else {
				pat = szSignature;
				firstMatch = 0;
			}
		}
		return NULL;
	}

};