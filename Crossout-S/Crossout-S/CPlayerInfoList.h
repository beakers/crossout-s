#pragma once

class PlayerInfo
{
public:
	int playerId; //0x0000 
	int uid; //0x0004 
	char nickName[16]; //0x1227888 
	char pad_0x0018[0x48]; //0x0018
	unsigned char isBot; //0x0060 
	char pad_0x0061[0x3]; //0x0061
	int status; //0x0064 
	int team; //0x0068 
	char pad_0x006C[0x40]; //0x006C
	int userLevel; //0x00AC 
}; 

class CPlayerInfoList
{
public:
	PlayerInfo* GetPlayerInfo(int32_t playerIdx)
	{
		return reinterpret_cast<PlayerInfo*>(reinterpret_cast<uintptr_t>(this) + ((188 * playerIdx) * 4));
	}
};