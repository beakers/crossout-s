#pragma once
#include "CPlayer.h"
class CCarList
{
public:
	CEntity* GetCarByIndex(int32_t Idx)
	{
		static auto get_car_by_index = reinterpret_cast<CEntity*(__thiscall*)(void*, int32_t)>(Utils::FindSignature("Crossout-D3D9.exe", "55 8B EC 69 45 ? ? ? ? ? 56"));
		return get_car_by_index(this, Idx);
	}

	CEntity* GetLocalPlayerCar()
	{
		static auto localplayer_index = *reinterpret_cast<int32_t*>(Utils::FindSignature("Crossout-D3D9.exe", "8B 0D ? ? ? ? 57 85 C9 78 0F 69 C1 ? ? ? ? 83 B8 ? ? ? ? ? 74 0A") + 2);

		return GetCarByIndex(*reinterpret_cast<int32_t*>(localplayer_index));
	}
};